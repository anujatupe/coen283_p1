   #include <string.h>
   #include <stdio.h>
   #include <stdlib.h>
   #include <ctype.h>

   /* Data Structure to store the tuples */

   union Data {
      int intnum;
      float f;
      char  str[20];
      char pattern[20];
      char tuple_template[20];
   };

   union Data global_tuple_array[20][20]; 

   /* Function to trim the leading and trailing whitespaces from the token which is passed to the function */

   char *trimwhitespace(char *str)
   {
     char *end;

     // Trim leading space
     while(isspace(*str)) str++;

     if(*str == 0)  // All spaces?
       return str;

     // Trim trailing space
     end = str + strlen(str) - 1;
     while(end > str && isspace(*end)) end--;

     // Write new null terminator
     *(end+1) = 0;

     return str;
   }

   /* Function to check if the token passed is an integer or not */

   int check_if_int(char *token) { 
      for (int count=0; count <= strlen(token); count++) {
         if(*token >= '0' && *token <= '9') {
            token++;
         } else {
            //printf("\nToken is not an int");
            return 0;
         }
      }
      printf("\nToken is int");
      return 1;
   }

   /* Function to check if the token passed is a float or not*/

   int check_if_float(char *token) { 
      int point_found = 0;
      for (int count=0; count <= strlen(token); count++) {
         if(*token >= '0' && *token <= '9') {
            token++;
         } else if ((*token == '.') && (!point_found)) {
            point_found = 1;
            token++;
         } else {
            //printf("\nToken is not a float");
            return 0;
         }
      }

      if (point_found == 1) {
         printf("\nToken is float");
         return 1;   
      } else {
         //printf("\nToken is not a float");
         return 0;
      }
   }

   /* Function to check if the token passed is a string or not */

   int check_if_string(char *token) {
      if ((token[0] == '\'' && token[strlen(token) - 1] == '\'' ) || ( token[0] == '"' && token[strlen(token) - 1] == '"')) {
         printf("\nToken %s is a string", token);
         return 1;
      } else {
         //printf("\nToken %s is not a string", token);
         return 0;
      }
   }

   /* Function which converts the token which is a string to integer */

   int convert_string_to_int (char *num) {
      int result;
      sscanf(num, "%d", &result);
      return result;
   }

   /* Function which converts the token which is a string to float */

   float convert_string_to_float (char *token) {
      float result;
      sscanf(token, "%f", &result);
      return result;
   }

   /* Function to check if the passed token is a valid linda command */

   int check_if_command(char *token) {
      if ((strcmp(token, "out") == 0) || (strcmp(token, "eval") == 0) || (strcmp(token, "in") == 0) || (strcmp(token, "rd") == 0) || (strcmp(token, "dump") == 0) || (strcmp(token, "inp") == 0) || (strcmp(token, "rdp") == 0) ) {
         printf("\nToken %s is a command", token);
         return 1;
      }
      //printf("\nToken %s is not a command", token);
      return 0;
   }

   /* Function is used to print the values in a single tuple */

   int print_single_tuple(union Data tuple[20]) {
      //printf("\n\nSINGLE TUPLE PRINTED:");
      for(int i=0; i<5; i++) {
         // printf("\nTuple template: %s", tuple[i].tuple_template);
         // printf("\nstr value: %s", tuple[i].str);
         // printf("\nint value: %d", tuple[i].intnum);
         // printf("\nfloat value: %f", tuple[i].f);
         // printf("\npattern value: %s", tuple[i].pattern);
      }
      return 1;
   }


   int add_correct_tuple_value(char datatype, union Data tuple[20], int out_position, int tuple_index) {
      if (tuple_index == 0) {
         strcpy(global_tuple_array[out_position][tuple_index].tuple_template, tuple[tuple_index].tuple_template);
         return 1;
      }
      //printf("\n\nDATATYPE: %c", datatype);
      switch(datatype) {
         case '1' :
            strcpy(global_tuple_array[out_position][tuple_index].str, tuple[tuple_index].str);
            break;
         case '2' :
            global_tuple_array[out_position][tuple_index].intnum = tuple[tuple_index].intnum;
            break;
         case '3' :
            //printf("\nFLOAT VALUE: %f", tuple[tuple_index].f);
            global_tuple_array[out_position][tuple_index].f = tuple[tuple_index].f;
            break;
         default:
            break;
      }
      return 1;
   }

   /* Function to remove a tuple from the global tuple space */

   void remove_tuple_from_global_tuple_space(int row_to_be_removed) {
      for (int col = 0; col < 20; col++) {
         strcpy(global_tuple_array[row_to_be_removed][col].str, "\0");
         strcpy(global_tuple_array[row_to_be_removed][col].tuple_template, "\0");
         strcpy(global_tuple_array[row_to_be_removed][col].pattern, "\0");
         global_tuple_array[row_to_be_removed][col].intnum = 0;
         global_tuple_array[row_to_be_removed][col].f = 0.0;
      }
   }

   int find_length_of_global_tuple_space() {
      int number_of_tuples = 0;
      for(int row = 0; row < 20; row++) {
         if(strlen(global_tuple_array[row][0].tuple_template) == 0) {
            break;
         }
         number_of_tuples++;
         
      }
      return number_of_tuples;
   }

   /* Helper function to get variable number */

   int get_variable_number(char variable_name[20], int variable) {
      int index_value;
      int is_a_number = 1;
      //printf("\n\nVARIABLE NUMBER IN STRAING FORMAT: %s", variable_name);
      if (variable) {
         index_value = 1;
      } else {
         index_value = 2;
      }
      for(int index = index_value; index<strlen(variable_name); index++) {
         if(variable_name[index] < '0' && variable_name[index] > '9') {
            is_a_number = 0;
            break;
         }
      }
      if(is_a_number) {
         if (variable) {
            return convert_string_to_int(&variable_name[1]);
         } else {
            return convert_string_to_int(&variable_name[2]);   
         }
         
      }
      return 0;
   }

   void display_str_int_double_array(char str_array[20][20], int int_array[20], float float_array[20]) {
      printf("\n\nSTRING VARIABLE: %s %s %s %s", str_array[0], str_array[1], str_array[2], str_array[3]);
      printf("\n\nINTEGER VARIABLE: %d %d %d %d", int_array[0], int_array[1], int_array[2], int_array[3]);
      printf("\n\nFLOAT VARIABLE: %f %f %f %f", float_array[0], float_array[1], float_array[2], float_array[3]);
   }

   /* 
      Helper function - Reads the appropriate tuple from the global tuple space.
      If remove_tuple = 1, removes it from the global tuple space after reading it.
      If remove_tuple = 0, doesn't remove it from the global tuple space after reading it.
      If blocking = 1 and if tuple is not found in the global tuple space, exits the program.
      If blocking = 0 and if tuple is not found, returns false to the calling function.
   */

   int read_tuple_from_global_tuple_space(union Data tuple[20], int remove_tuple, int blocking, char str_array[20][20], int int_array[20], float float_array[20]) {
      // Keep a copy of the original tuple_template from the single tuple array.
      char temp_tuple_template[20];
      strcpy(temp_tuple_template, tuple[0].tuple_template); 
      // This tuple_template will be used to compare with the tuple_templates which are stored with each tuple array in the global tuple space
      char tuple_template_to_match[20];
      // Variable to keep a track of whether the elements in the tuple are correct.
      int right_so_far = 0;
      // We will be converting the original tuple_template to the format which is required by the global tuple space. 
      // Meaning, global tuple space has tuple templates which have only 1 stands for string/2 stands for int/3 stands for float as their characters.
      // Original tuple templates have 4 for var pattern and  5 for actual variable name which will be used in "in", "rd", "inp", "rdp" commands
      //printf("\n\n TEMP TUPLE TEMPLATE AND ITS LENGTH: %s , %lu", temp_tuple_template, strlen(temp_tuple_template));
      for (int index = 0; index < 20 ; index++) {
         if(temp_tuple_template[index] == '4') {
            if(tuple[index+1].str[1] == 's') {
               tuple_template_to_match[index] = '1';
            } else if (tuple[index+1].str[1] == 'i') {
               tuple_template_to_match[index] = '2';
            } else if (tuple[index+1].str[1] == 'd') {
               tuple_template_to_match[index] = '3';
            }
         } else {
            tuple_template_to_match[index] = temp_tuple_template[index];
         }
      }

      //printf("\n\n THE IN COMMAND TUPLE: ");
      //print_single_tuple(tuple);
      //printf("\n\n TUPLE TEMPLATE AND ITS LENGTH: %s , %lu", tuple_template_to_match, strlen(tuple_template_to_match));

      // This for loop uses the tuple_template_to_match tuple template to check if the input tuple template is matching the tuple template
      // of any of the tuples present in the global tuple space. If the tuple template matches, we check if it exactly matches the commands pattern.
      int row = 0;
      int col;
      for(col = 0; col < 20; col++) {
         // Starting over a new row in the global array so set the check element to zero
         right_so_far = 0;
         if(strcmp(global_tuple_array[col][row].tuple_template, tuple_template_to_match) == 0) {
            //check if futher values are correct
            for (int row = 1; row <= strlen(tuple_template_to_match) ; row++ ) {
               switch(temp_tuple_template[row-1]) {
                  case '1' :
                     {
                        if(strcmp(tuple[row].str, global_tuple_array[col][row].str) == 0) {
                           right_so_far = 1;
                        } else {
                           right_so_far = 0;  
                        }
                        break;
                     }
                  case '2' :
                     {
                        if(tuple[row].intnum == global_tuple_array[col][row].intnum) {
                           right_so_far = 1;
                        } else {
                           right_so_far = 0;
                        }
                        break;
                     }
                  case '3' :
                     {
                        if(tuple[row].f == global_tuple_array[col][row].f) {
                           right_so_far = 1;
                        } else {
                           right_so_far = 0;
                        }
                        break;
                     }
                  case '4' :
                     {  
                        //printf("\n\n VAR DATATYPE FIRST CHARACTER %c",tuple[row].str[1]);
                        int variable_number = 0;
                        variable_number = get_variable_number(tuple[row].str, 0);
                        //printf("\n\n\n\n\nVARIABLE NUMBER IN READ: %d", variable_number);
                        if(tuple[row].str[1] == 's') {
                           right_so_far = 1;
                           // store the string value in some varible named str_array
                           // variable_number will be the value tuple[row].str[2-endofstring]
                           strcpy(str_array[variable_number] , global_tuple_array[col][row].str);
                        } else if (tuple[row].str[1] == 'i') {
                           // store the string value in some varible named int_array
                           int_array[variable_number] = global_tuple_array[col][row].intnum;
                           right_so_far = 1;
                        } else if (tuple[row].str[1] == 'd') {
                           // store the string value in one variable named float_array
                           float_array[variable_number] = global_tuple_array[col][row].f;
                           right_so_far = 1;
                        } else {
                           right_so_far = 0;
                        }
                        break;
                     }
                  default :
                     right_so_far = 0;
                     break;
               }

               if(right_so_far == 0) {
                  break;
               }
               
            }
         }
         if (right_so_far == 1) {
               break;
         }
      }
      display_str_int_double_array(str_array, int_array, float_array);
      if (right_so_far == 1) {
         // If remove_tuple is 1, it is an "in" command, else it is "rd" command
         // TODO - implement inp and rdp.
         if (remove_tuple == 1) {
            // Remove the element from the global tuple space
            //display_str_int_double_array(str_array, int_array, float_array);
            remove_tuple_from_global_tuple_space(col);
         } 
         printf("\n\nTuple found in global space");
         return 1;
      } else {
         printf("\n\nTuple not found in global space");
         return 0;
      }
      
   }

   /* Implementation of the in command in linda - Makes use of helper function read_tuple_from_global_tuple_space */

   int in_command(union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {
      int found = read_tuple_from_global_tuple_space(tuple, 1, 1, str_array, int_array, float_array);
      if (found == 0) {
         printf("\n\nEXITING THE PROGRAM");
         exit(1);
      } else {
         return 1;
      }
   }

   /* Implementation of the rd command in linda - Makes use of the helper function read_tuple_from_global_tuple_space */

   int rd_command(union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {
      int found = read_tuple_from_global_tuple_space(tuple, 0, 1, str_array, int_array, float_array);
      if (found == 0) {
         printf("\n\nEXITING THE PROGRAM");
         exit(1);
      } else {
         return 1;
      }
   }

   /* Implementation of the inp command in linda - Makes use of helper function read_tuple_from_global_tuple_space */

   int inp_command(union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {
      int found = read_tuple_from_global_tuple_space(tuple, 1, 0, str_array, int_array, float_array);
      if (found == 0) {
         printf("\n\nINP COMMAND : %s", "false");
         return 0;
      } else {
         printf("\n\nINP COMMAND : %s", "true");
         return 1;
      }
      
   }

   /* Implementation of the rdp command in linda - Makes use of the helper function read_tuple_from_global_tuple_space */

   int rdp_command(union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {
      int found = read_tuple_from_global_tuple_space(tuple, 0, 0, str_array, int_array, float_array);
      if (found == 0) {
         printf("\n\nRDP COMMAND : %s", "false");
         return 0;
      } else {
         printf("\n\nRDP COMMAND : %s", "true");
         return 1;
      }
   }


   /* Implementation of the out command in linda */

   int out_command(union Data tuple[20]) {
      int out_position = 0;
      char tuple_template[20];
      int tuple_length =0;

      strcpy(tuple_template, tuple[0].tuple_template);
      printf("\n\nCOPIED TUPLE TEMPLATE: %s ", tuple_template);

      //print_single_tuple(tuple);

      for (int j=0; j< 20; j++) {
         /* FIXME - INITIALIZE INT AND FLOAT TO SOMETHING ELSE BECAUSE 0 AND 0.0 ARE VALID TUPLE ELEMENTS */
         if ( (strcmp(global_tuple_array[j][0].tuple_template ,"\0") == 0) && (strcmp(global_tuple_array[j][0].str ,"\0") == 0) && global_tuple_array[j][0].intnum == 0 && global_tuple_array[j][0].f == 0.0 && (strcmp(global_tuple_array[j][0].pattern, "\0") == 0)) {
             out_position = j;
             break;
         }
      }

      //printf("\nOUT POSITION BEFORE FOR LOOP: %d", out_position);

      /* FIXME - INITIALIZE INT AND FLOAT TO SOMETHING ELSE BECAUSE 0 AND 0.0 ARE VALID TUPLE ELEMENTS */
      // you start with 1 as 0th element is the tuple template - 123

      // subtract one because we do not consider the zeroth element which is used to store the tuple_template and we start from 1st element of the tuple array
      tuple_length = strlen(tuple[0].tuple_template) + 1;
      //printf("\n\nTUPLE LENGTH IS: %d", tuple_length);

      add_correct_tuple_value(tuple_template[0], tuple, out_position, 0);
      for (int each = 0; each<=tuple_length ; each++) {
         //printf("\nTUPLE BEFORE PUTTING IN GLOBAL VARIABLE: %s %d %f", tuple[each].str, tuple[each].intnum, tuple[each].f);
         add_correct_tuple_value(tuple_template[each], tuple, out_position, each+1);
         // strcpy(global_tuple_array[out_position][each].str, tuple[each].str);
         // strcpy(global_tuple_array[out_position][each].pattern, tuple[each].pattern);
         // global_tuple_array[out_position][each].intnum = tuple[each].intnum;
         // global_tuple_array[out_position][each].f = tuple[each].f;
      }

      printf("\n\nTuple added to the global tuple space");
      return 1;
      //printf("\nOUT POSITION AFTER FOR LOOP: %d", out_position);

   }

   /* Initialize the global tuple space */

   void initialize_global_tuple_space() {

      /* FIXME - INITIALIZE INT AND FLOAT TO SOMETHING ELSE BECAUSE 0 AND 0.0 ARE VALID TUPLE ELEMENTS */
      for (int i=0; i<20; i++) {
         for(int j=0; j<20; j++) {
            strcpy(global_tuple_array[i][j].str , "\0");
            strcpy(global_tuple_array[i][j].pattern , "\0");
            strcpy(global_tuple_array[i][j].tuple_template , "\0");   
            global_tuple_array[i][j].intnum = 0;
            global_tuple_array[i][j].f = 0.0;
         }
      }
   }

   /*
      Function to evaluate the value before using it in the out function.
   */

   // void eval_command(tuple, str_array, int_array, float_array) {
   // }

   /* Helper function for the dump function */

   int print_element_of_tuple(char datatype, union Data tuple[20], int tuple_index) {
      if (tuple_index == 0) {
         // do nothing as this element has nothing except the tuple pattern
         return 1;
      }
      
      switch(datatype) {
         case '1' :
            printf("%s ",tuple[tuple_index].str);
            break;
         case '2' :
            printf("%d ",tuple[tuple_index].intnum);
            break;
         case '3' :
            printf("%f ",tuple[tuple_index].f);
            break;
         default:
            break;
      }
      return 1;
   }

   /* Function to print all the tuples in the global tuple space */

   int dump_global_tuple_space() {
      
      // Iterates through the global tuple space and prints the tuples in the required format.
      //printf("\n\nInside the dump function:");
      int i =0;
      for (i=0; i<20; i++) {
         //printf("\n i = %d", i);
         if(strlen(global_tuple_array[i][0].tuple_template) > 0) {
            printf("\n( ");
            for (int j=1; j<20; j++) {
               print_element_of_tuple(global_tuple_array[i][0].tuple_template[j-1], global_tuple_array[i], j);
            }
            printf(" );");
         }
      }
      //printf("\nVALUE OF I: %d", i);
      //printf("\n RETURNING FROM DUMP");
      return 1;
   }

   /* Function to execute the linda command */

   int execute_linda_command(char *command, union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {
      int ret_val = 0;
      if ((strcmp(command, "out") == 0)) {
         ret_val = out_command(tuple);
      } else if ((strcmp(command, "in") == 0)) {
         ret_val = in_command(tuple, str_array, int_array, float_array);
      } else if ((strcmp(command, "rd") == 0)) {
         ret_val = rd_command(tuple, str_array, int_array, float_array);
      } else if ((strcmp(command, "inp") == 0)) {
         ret_val = inp_command(tuple, str_array, int_array, float_array);
      } else if ((strcmp(command, "rdp") == 0)) {
         ret_val = rdp_command(tuple, str_array, int_array, float_array);
      } else if((strcmp(command, "dump") == 0)) {
         ret_val = dump_global_tuple_space();
      } else if((strcmp(command, "eval") == 0)) {
         //eval_command(tuple, str_array, int_array, float_array);
         //make a call to eval along with file name where function will be stored.
      }
      return ret_val;
   }




   /* Function to initialize the single tuple array - this array stores the value of the recent token*/

   int initialize_single_tuple_array(union Data tuple[20]) {
      for (int i=0; i<20; i++) {
         strcpy(tuple[i].str , "\0");
         tuple[i].intnum = 0;
         tuple[i].f = 0.0;
         strcpy(tuple[i].pattern, "\0");
         strcpy(tuple[i].tuple_template, "\0");
      }
      return 1;
   }

   /* Function to check if the token is a variable pattern - used with in, rd, inp and rdp command. For eg - ?d1, ?i2 */

   int check_if_var_pattern(char *token) {
      if((token[0] == '?') && ((token[1] == 's') || (token[1] == 'd') || (token[1] == 'i') ) ) {
         printf("\nToken %s is a variable pattern", token);
         return 1;
      }
      return 0;
   }

   /* Function to check if the token is a variable name - used with out. */

   int check_if_variable(char *token) {
      if(((token[0] == 's') || (token[0] == 'd') || (token[0] == 'i'))) {
         printf("\nToken %s is a variable", token);
         return 1;
      }
      return 0;
   }

   /* Function to check if the token indicates that it is a user defined function */

   int check_if_user_defined_fn(char *token) {
      char define_keyword[20];
      for(int i =0; i< 6 ; i++) {
         define_keyword[i] = token[i];
      }
      if(strcmp(define_keyword, "define") == 0) {
         printf("\nToken %s is a user defined function ", token);
         return 1;
      }
      return 0;
   }


   /* Create a c file for the user defined function which is inside the input file */


   void create_c_file(char contents[1000], char output_file_name[100], char mk_file_name[100]) {
      char systemcall[100];
      char run_command[100];
      int result = 0;
      printf("\n\nContents to be written to %s file : %s", output_file_name ,contents);
      printf("\n\nWriting user defined function to file %s", output_file_name);
      FILE *ccf = fopen(output_file_name, "w");
      //char main_fn [200] = "int main() { return 0; }";
      int ret_val = fwrite(contents , 1 , strlen(contents) , ccf );
      //printf("\n\nret value of fwrite is: %d", ret_val);
      fclose(ccf);
      /*ccf = fopen(output_file_name, "a");
      ret_val = fwrite(main_fn , 1 , strlen(main_fn) , ccf );
      fclose(ccf);*/
      strcpy(systemcall, "gcc ");
      strcat(systemcall, output_file_name);
      strcat(systemcall, " -o ");
      strcat(systemcall, mk_file_name);
      system(systemcall);
      // strcpy(run_command, "./");
      // strcat( run_command, mk_file_name);
      // strcat(run_command, " 3");
      // result = system(run_command);
      // printf("\n\nTHE result is : %d", result);
   }


   /* Read a c file which has the user defined function */


   FILE* read_c_file(char *fileName, char *c_file_name)
   {
      FILE *file;
      int open_braces = 1;
      char *code = malloc(1000 * sizeof(char));
      char contents[1000];
      file = fopen(fileName, "r");
      int close_braces = 0;
      int j = 0;
      int k = 0;
      char discard[100];
      char output_file_name[100];
      char mk_file_name[100];
      strcpy(contents, "\0");
      strcpy(output_file_name, "\0");
      strcpy(mk_file_name, "\0");

      //printf("\n\n c file name: %s \n\n", c_file_name);
      trimwhitespace(c_file_name);
      c_file_name = strtok(c_file_name, " (");
      c_file_name = strtok(NULL, " (");
      c_file_name = strtok(NULL, " (");

      strcpy(output_file_name, "my_");
      strcat(output_file_name, c_file_name);
      strcpy(mk_file_name, output_file_name);
      strcat(output_file_name, ".c");

      //printf("\n\nC FILENAME: %s \n\n", c_file_name);
      //printf("\n\nOUTPUT fFILE: %s \n\n", output_file_name);

      for(int k=0; k<1000; k++) {
         contents[k] = '\0';
      }


      /*This commented code replaces the function name with int main and then writes it to the file */
      char var_name[100];
      int m = 0;
      
      do {
         discard[k] = (char)fgetc(file);
         //printf("\nDISCARD CHAR: %c", discard[k]);
         
         // remove trailing and leading white spaces
         //trimwhitespace(discard);

         if (discard[k] == '(') {
            do {
               var_name[m] = (char)fgetc(file);
               if(var_name[m] == ')') {
                  break;
               }
               m++;
            } while (!feof(file));
            var_name[m] = '\0';

            strcat(var_name, " = atoi(argv[1]);");

            //printf("\n\nVARIABLE NAME: %s", var_name);
            //break; 
         }

         if (discard[k] == '{') {
            break;
         }
         
         k++;
         //printf("DISCARD LENGTH: %lu", strlen(discard));
         // if(strstr(discard, "define") != NULL) {
         //    break;
         // }
      } while(!feof(file));
       
      strcpy(contents, "#include<stdlib.h> \n int main (int argc, char* argv[]) { \n ");
      strcat(contents, var_name);
      j = strlen(contents);

      

      do {
         *code = (char)fgetc(file);
         contents[j] = *code;
         //printf("\nchar is: %c", *code);
         if(*code == '{') {     
            open_braces++; 
         } else if (*code == '}') {
            close_braces++;
         }
         code++;
         j++;

      } while((!feof(file)) && (((open_braces == close_braces) && (open_braces == 0) && (close_braces == 0)) || (open_braces != close_braces)));


      //printf("\n\ncontents : %s", contents);
      create_c_file(contents, output_file_name, mk_file_name);
      return file;
   }


   int check_if_fn_call(char *token) {
      int token_len = strlen(token);
      char fn_arg[5]; 
      char fn_name[10];
      int j =0;
      int bracket = 0;

      if(strstr(token, "(")){
         bracket++;
      }

      if(strstr(token, ")")) {
         bracket++;
      }

      if(bracket == 2) {
         return 1;
      } else {
         return 0;
      }
   }

   int execute_user_defined_function(char *token) {
      int token_len = strlen(token);
      char fn_arg[5]; 
      char fn_name[10];
      int j =0;
      int bracket = 0;
      int k =0;

      strcpy(fn_name, "\0");
      strcpy(fn_arg, "\0");

      //printf("\n\nFUNCTION NAME : %s", fn_name);
      //printf("\n\nFUNCTION ARGS: %s", fn_arg);

      for(int i = 0; i<token_len; i++) {
         //printf("\n char: %c", token[i]);
         if (token[i] == '(') {
            i++;
            while(token[i] != ')') {
               //printf("\nAdding this in arg: %c", token[i]);
               fn_arg[j] = token[i];
               j++;
               i++;
            }
            fn_arg[j] = '\0';
            break;
         }
         fn_name[k] = token[i];
         k++;
         //printf("\nAdding this in function name: %c", token[i]);
      }

      fn_name[k] = '\0';


      //printf("\n\nFUNCTION NAME : %s", fn_name);
      //printf("\n\nFUNCTION ARGS: %s", fn_arg);

      //int int_arg = 0;
      
      int result = 0;
      char run_command[100];

      strcpy(run_command, "\0");
      //sscanf(fn_arg, "%d", &int_arg);

      strcpy(run_command, "./my_");
      strcat(run_command, fn_name);
      strcat(run_command, " ");
      strcat(run_command, fn_arg);
      //printf("\n\nRUN COMMAND: %s", run_command);
      result = system(run_command);
      //printf("\n\nTHE result is : %d", result);
      return result;

   }

   int execute_eval_command(char *command, char *str_copy, union Data tuple[20], char str_array[20][20], int int_array[20], float float_array[20]) {

      char *token;
      //printf("\nEXECUTING EVAL FUNCTION");   
      trimwhitespace(command);
      trimwhitespace(str_copy);
      
      
      int command_len = strlen(command);
      //printf("\n\nCOMMAND LEN: %d", command_len);
      char eval_args[300];
      char temp_tuple_template[20];
      strcpy(temp_tuple_template, "\0");

      strcpy(eval_args, &str_copy[command_len]);

      //printf("\n\nEVAL CPIED STR: %s", eval_args);
      trimwhitespace(eval_args);
      //printf("\n\nEVAL CPIED STR: %s", eval_args);
      int eval_args_len = strlen(eval_args);
      eval_args[eval_args_len - 1] = ' ';
      trimwhitespace(eval_args);
      eval_args[0] = ' ';
      eval_args[eval_args_len - 2] = ' ';
      //printf("\n\nEVAL CPIED STR: %s", eval_args);
      int i = 1;
      token = strtok(eval_args, ",\n");
      
      // walk through the rest of the tokens
      while( token != NULL ) {
         int variable_number = 0;
         token = trimwhitespace(token);
         if (check_if_fn_call(token)) {
            int result = execute_user_defined_function(token);
            tuple[i].intnum = result;
            strcat( temp_tuple_template, "2");
            i++;
         } else if (check_if_variable(token)) {
            variable_number = get_variable_number(token, 1);
            strcpy(tuple[i].str, "\0");
            if(token[0] == 's') {
               strcpy(tuple[i].str, str_array[variable_number]);
               strcat( temp_tuple_template, "1");
            } else if(token[0] == 'i') {
               tuple[i].intnum = int_array[variable_number];
               strcat( temp_tuple_template, "2");
            } else if(token[0] == 'd') {
               tuple[i].f = float_array[variable_number];
               strcat( temp_tuple_template, "3");
            }
            i++;
         } else if (check_if_string(token)) {
            strcpy( tuple[i].str, token);
            strcat( temp_tuple_template, "1");
            i++;
         } else if (check_if_int(token)) {
            tuple[i].intnum = convert_string_to_int(token);
            strcat( temp_tuple_template, "2");
            i++;
         } else if (check_if_float(token)) {
            tuple[i].f = convert_string_to_float(token);
            //printf("\nFLOAT VALUE WHEN ENTERING: %f", tuple[i].f);
            strcat( temp_tuple_template, "3");
            i++;
         } else {
            printf("\nTuple is not in the correct format.");
            break;
         }



         token = strtok(NULL, ",\n");
      }

   strcpy(tuple[0].tuple_template, temp_tuple_template);
   execute_linda_command("out", tuple, str_array, int_array, float_array);

   return 0;
   }


   /* Function to tokenize the linda command read from the file */

   int tokenize_command_line(char *str, char str_array[20][20], int int_array[20], float float_array[20], char *filename) {
      // variables used in the linda command are stored here. For eg - d1, i2, etc. 
      // Actually these variables are supposed to be created in the pthread create function which will inturn call 
      // tokenize_command_line function. So, basically the below three arrays will be parameters for the tokenize_command_line function 
      // char str_array[20][20];
      // int int_array[20]; 
      // float float_array[20];
      //char trial_string[100] = "out(\"anuja\", 2, 5.0, 11.0);\n";
      // char pointer to store the token and go through the entire string which has to be tokenzied.
      char str_copy[300];
      strcpy(str_copy, str);
      char *token;
      // Command should be present only once in the line. Keep a flag to keep a track of the command
      int command_found = 0;
      // The tokenzied command string is stored in this variable
      char *command;
      // Allocated space for the command char pointer
      command = malloc(sizeof(char) * 10);
      // Temporary union array to store the tokenized tuple - this array only has the tuple values i.e. it eliminates the linda command   
      union Data tuple[20];  
      // 'i' is used as an index iterator for the union tuple array. 
      // Variable i is used to move ahead in the temporary array to store the next value of tuple.
      int i =1;
      // Temporary tuple template
      char temp_tuple_template[20];
      // Temporary flag to check if define keyword was encountered. If yes, then we break the while loop and we parse that file separately.
      int break_loop = 0;
      // FILE pointer to the file if it is a file having user defined functions or if/for commands
      FILE *code_file_pointer;
      int tokenize_ret_val = 0;
      strcpy(temp_tuple_template, "\0");

      //Initialize the tuple array before the loop starts.
      initialize_single_tuple_array(tuple);

      //printf("\n\n***********************NEW COMMAND**********************");
      printf("\nCOMMAND LINE IS : %s", str);

      // First call to strtok to tokenize the string read from the file
      token = strtok(str, "(,);\n");
      
      // walk through the rest of the tokens
      while( token != NULL ) {
         printf( "\nTOKEN is :%s", token );

         // Initialize the variable number to zero 
         int variable_number = 0;

         // Trim the leading and trailing whitespaces from the token before you check if it is a valid token
         token = trimwhitespace(token);

         // check if it is command - out, in, rd, rdp, inp, eval
         if(check_if_user_defined_fn(token)) {
            printf("\n\nUSER DEFINED FUNCTION FILENAME IS : %s", filename);
            code_file_pointer = read_c_file(filename, token);
            break_loop = 1;
            break;
         } else if ((check_if_command(token)) && (command_found == 0) && (i == 1)) {
            command_found = 1;
            strcpy(command, token);
            if(strcmp(command, "eval") == 0) {
               execute_eval_command(command, str_copy, tuple, str_array, int_array, float_array);
               break_loop = 1;
               break;
            }
         } else if((command_found == 1) && (check_if_var_pattern(token))) {
            strcpy( tuple[i].str, token);
            strcat( temp_tuple_template, "4");
            i++;
         } else if((command_found == 1) && (check_if_variable(token))) {
            // Instead of doing the below commands, get the variable value and store it in the token and 
            // change the strcat command to concatenate "1" , "2", "3" 
            //strcpy( tuple[i].str, token);
            //strcat( temp_tuple_template, "5");
            variable_number = get_variable_number(token, 1);
            //printf("\n\nVARIABLE NUMBER IN TOKENIZE: %d", variable_number);
            //display_str_int_double_array(str_array, int_array, float_array);
            strcpy(tuple[i].str, "\0");
            if(token[0] == 's') {
               strcpy(tuple[i].str, str_array[variable_number]);
               strcat( temp_tuple_template, "1");
            } else if(token[0] == 'i') {
               tuple[i].intnum = int_array[variable_number];
               strcat( temp_tuple_template, "2");
            } else if(token[0] == 'd') {
               tuple[i].f = float_array[variable_number];
               strcat( temp_tuple_template, "3");
            }
            i++;
         } else if ((command_found == 1) && (check_if_string(token))) {
            strcpy( tuple[i].str, token);
            strcat( temp_tuple_template, "1");
            i++;
         } else if ((command_found == 1) && check_if_int(token)) {
            tuple[i].intnum = convert_string_to_int(token);
            strcat( temp_tuple_template, "2");
            i++;
         } else if ((command_found == 1) && check_if_float(token)) {
            tuple[i].f = convert_string_to_float(token);
            printf("\nFLOAT VALUE WHEN ENTERING: %f", tuple[i].f);
            strcat( temp_tuple_template, "3");
            i++;
         } else {
            printf("\nTuple is not in the correct format.");
            break;
         }

         token = strtok(NULL, "(,);\n");
      }

      if (break_loop == 0) {
         strcpy(tuple[0].tuple_template, temp_tuple_template);

         // The last element in the array of tuples should be set to null to indicate end of the array. 
         // This is done by setting the string part of the union to a null character, integer part of the 
         /* FIXME - INITIALIZE INT AND FLOAT TO SOMETHING ELSE BECAUSE 0 AND 0.0 ARE VALID TUPLE ELEMENTS */
         // strcpy(tuple[i].tuple_template, "\0");
         // strcpy(tuple[i].str, "\0");
         // strcpy(tuple[i].pattern, "\0");
         // tuple[i].int = 0;
         // tuple[i].f = 0.0;

         print_single_tuple(tuple);
         tokenize_ret_val = execute_linda_command(command, tuple, str_array, int_array, float_array);
         printf("\n\nGLOBAL TUPLE SPACE:");
         dump_global_tuple_space();
         //printf("\n\nUNION PRINTED : %s %s %s %d %f \n\n", command, tuple[0].tuple_template, tuple[1].str, tuple[2].intnum, tuple[3].f);

         //call the apropriate command function.   
         //code_file_pointer = NULL;
         //printf("\nTOKENIZE RET VAL: %d", tokenize_ret_val);
         return tokenize_ret_val;
      } else {
         return 1;
      }
   }

   /* Tokenizes the string passed and returns the first token of the string*/
   char *get_first_token(char *line) {
      //printf("\n\nGETTING FIRST TOKEN");
      char *token;
      //strcpy(token, "\0");
      token = strtok(line, "(,);\n");
      token = trimwhitespace(token);
      return token;
   }

   /* Function which executes the body of if, if the condition is true */
   int execute_if_body(char if_body[200], char str_array[20][20], int int_array[20], float float_array[20], char *filename) {
      printf("\n\nIF BODY: %s", if_body);
      char instructions_array[40];
      int i = 0;
      //printf("\n\nTRSING LENGTH OF IF BODY: %lu", strlen(if_body));
      trimwhitespace(if_body);
      for(int j = 0; j<strlen(if_body); j++) {
         //printf("\n\nVALUE OF J: %d", j);
         if ((if_body[j] != '{') && (if_body[j] != '}')) {
            instructions_array[i] = if_body[j];
            i++;
            if(if_body[j] == ';') {
               instructions_array[i] = '\0';
               i = 0;
               //printf("\n\nIF BODY INSTRUCTION: %s", instructions_array);
               int ret_val = tokenize_command_line(instructions_array, str_array, int_array, float_array, filename);
               //printf("\n\nRET VAL: %d", ret_val);
            }

         }
      }
      return 0;
   }


   FILE* for_command(char instruction_string[], FILE *file_pointer, char str_array[20][20], int int_array[20], float float_array[20], char *filename){
     char for_string[200];
     int index = 0;

     printf("\nFOR INSTRUCTION STRING IS: %s", instruction_string);
     for(unsigned long i = 0; i < strlen(instruction_string); i++){
       if(instruction_string[i] != ' '){
         if(instruction_string[i] != '{'){
           for_string[index++] = instruction_string[i];
         }else if(instruction_string[i] == '{'){
           for_string[index++] = '\0';
           break;
         }
       }
     }

     char for_block_buffer[400];
     strcpy(for_block_buffer, "");
     int block_brace_counter = 0;
     char current_char;
     index = 0;
     fseek(file_pointer, -strlen(instruction_string), SEEK_CUR);

     while(!(feof(file_pointer))){
       current_char = fgetc(file_pointer);

       if(current_char == '{'){
         block_brace_counter++;
       }

       if(block_brace_counter > 0){
         for_block_buffer[index++] = current_char;
       }

       if(current_char == '}'){
         block_brace_counter--;
         for_block_buffer[index] = '\0';
         break;
       }
     }

     //printf("\nthe for string is %s", for_string);

     char *token;
     char for_token_pattern[200] = "();";
     char initial_for_condition[200];
     char for_condition[200];
     int for_token_index = 0;

     initial_for_condition[0] = '\0';
     for_condition[0] = '\0';

     printf("\nthe for string is %s", for_string);

     token = strtok(for_string, for_token_pattern);
     //printf("\nFIRST TOKEN: %s", token);
     while(token != NULL){
       if(for_token_index == 1){
         strcpy(initial_for_condition, token);
       }else if(for_token_index == 2){
         strcpy(for_condition, token);
       }
       token = strtok(NULL, for_token_pattern);
       //printf("\nNEXT TOKEN: %s", token);
       for_token_index++;
     }

     // printf("the for string is %s", for_string);

     // printf("\n For condition is: %s \n the for initial is: %s", for_condition, initial_for_condition);

     // printf("\n\n The for block is: %s", for_block_buffer);

     char *for_init_char;
     for_init_char = strtok(initial_for_condition, "=");
     for_init_char = strtok(NULL, "=");

     int for_init = 0;
     sscanf(for_init_char, "%d", &for_init);

     char *for_cond_char;
     for_cond_char = strtok(for_condition, "<>");

     for_cond_char = strtok(NULL, "<>");

     int for_final_int = 0;
     sscanf(for_cond_char, "%d", &for_final_int);

     char for_instruction_string[100];
     int instruction_index = 0;
     //printf("\n for init value: %d", for_init);
     //printf("\n for condition value: %d", for_final_int);
     for(int counter = for_init; counter < for_final_int; counter++){
         execute_if_body(for_block_buffer, str_array, int_array, float_array, filename);
     }

     return file_pointer;
   }

   FILE *if_command(char *line, FILE *fp, char str_array[20][20], int int_array[20], float float_array[20], char *filename) {
      //printf("\n\nEntered if command");
      char if_condition[100];
      strcpy(if_condition, "");
      int bracket = 0;
      int if_condition_output = 0;
      int if_cond_len = 0;

      char if_body[200];
      for(int b = 0; b < 200; b++){
        if_body[b] = '\0';
      }
      int if_body_index = 0;
      int if_body_brackets = 0;
      int if_condition_len = strlen(line);

      for(unsigned long i = 0; i < (strlen(line)); i++){
          if(strncmp(&line[i], ")", 1) == 0){
            bracket--;
          }

          if(bracket >= 1){
            strncat(if_condition, &line[i], 1);
          }

          if(strncmp(&line[i], "(", 1) == 0){
            bracket++;
          }
      }
      if_cond_len = strlen(if_condition);
      if_condition[if_cond_len-1] = ';';
      //strncat(if_condition, ";", 1);
      if_condition_output = tokenize_command_line(if_condition, str_array, int_array, float_array, filename);
      printf("\n\n IF CONDITION OUTPUT IS %d", if_condition_output);

      printf("\n\nIF CONDITION: %s", if_condition);

      if(if_condition_output == 1) {
         

         if_condition_len = -if_condition_len;
         fseek(fp, if_condition_len, SEEK_CUR);
         while(!(feof(fp))){

           char current_char = fgetc(fp);

           if( current_char == '{'){
             if_body_brackets++;
           }

           if(if_body_brackets > 0){
             if_body[if_body_index] = current_char;
             if_body_index++;
             //if_body_brackets++;
           }

           if(current_char == '}'){
             if_body_brackets--;
             break;
           }
         }
         printf("\nIF CONDITION IS TRUE: %s", if_body);
         execute_if_body(if_body, str_array, int_array, float_array, filename);

         /* start - code to move pointer to end of else if else is present */

         char current_char;
         char else_str[100];
         int else_index = 0;

         while(!(feof(fp))) {
            current_char = fgetc(fp);
            else_str[else_index++] = current_char;

            if(current_char == '\n'){ // the else line has ended
               else_str[else_index] = '\0';
               break;
            }
         }

         if(strstr(else_str, "else") != NULL){
            while(!(feof(fp))){
               if(fgetc(fp) == '}'){ // go on until else brace closes
                   fgetc(fp);
                  break;
               }
            }
         }else{
            fseek(fp, -strlen(else_str), SEEK_CUR);
         }

         /* end - code to move pointer to end of else if else is present */

      } else if (if_condition_output == 0) {
         int execute_else_block = 0;
         char else_block[200];
         char else_str[100];
         int else_index = 0;
         char current_char;
         int else_block_index = 0;
         int else_block_brackets = 0;
         int else_block_flag = 0;

         for(int b = 0; b < 200; b++){
           else_block[b] = '\0';
         }

         while(!(feof(fp))){
           current_char = fgetc(fp);

           if((current_char == '}') && (else_block_flag == 0)){ // sees if else is present
             else_block_flag = 1;
             char c;

             while(!(feof(fp))){
               if(c == '\n'){
                  else_str[else_index] = '\0';
                 break;
               }

               c = fgetc(fp);
               else_str[else_index++] = c;
             }

             if(strstr(else_str, "else") != NULL){
               printf("\n\n ELSE BLOCK NEEDS EXECUTION!!");
               execute_else_block = 1;
               fseek(fp, -strlen(else_str), SEEK_CUR);
               printf("\n\n\n TEMP HAS: %s", else_str);
               current_char = fgetc(fp);
             }else{
               printf("\n\nBREAKING BAD BECAUSE NO ELSE BLOCK PRESENT\n\n");
               break;
             }

           } // see if else is present

           if(execute_else_block == 1){
               //printf("\n char is: %c",  current_char);
             if(current_char == '{'){
               else_block_brackets++;
             }

             if(else_block_brackets > 0){


               else_block[else_block_index] = current_char;
               else_block_index++;
             }

             if(current_char == '}'){
               else_block_brackets--;
               if(else_block_brackets == 0){
                 break;
               }
             }
             
           }
         }
         printf("\nELSE BLOCK STRING: \n%s", else_block);
         printf("\n\nEXECUTING THE ELSE PART> CONDITION WAS FALSE");
         execute_if_body(else_block, str_array, int_array, float_array, filename);
         
      }
      return fp;
   }

   /* Function to read the Linda commands in the input files */

   void read_file_contents(char *filename, char str_array[20][20], int int_array[20], float float_array[20]) {
      printf("\nReading the input files and executing the linda commands from : %s \n", filename);

      char *pos;
      char *first_token;
      int if_st_len = 0;
      //int tokenize_ret_val = 0;
      if((pos= strchr(filename, '\n')) != NULL)
         *pos = '\0';

      FILE *file_pointer;
      file_pointer = fopen(filename, "r");

      if(file_pointer != NULL) {
         char line[300];
         char copied_line[300];
         while(fgets(line, sizeof(line), file_pointer) != NULL){
            //printf("\n\nSTARTING LOOP AGAIN ");
            //tokenize_ret_val =  tokenize_command_line(line, str_array, int_array, float_array, filename);
            printf("\n\n***********************NEW COMMAND**********************");
            strcpy(copied_line, "\0");
            strcpy(copied_line, line);
            printf("\n\nLINE IS: %s", line);
            //printf("\n\nCOPIED LINE IS: %s", copied_line);
            trimwhitespace(copied_line);
            //first_token = get_first_token(copied_line);
            //printf("\n\nFIRST TOKEN: %s", first_token);
            if((strlen(copied_line) != 1) && (strcmp(copied_line, "\n") != 0)) {
               if(strncmp(copied_line, "if", 2) == 0) {
                  
                  file_pointer = if_command(line, file_pointer, str_array, int_array, float_array, filename);

               } else if(strncmp(copied_line, "for", 3) == 0) {
                  file_pointer = for_command(line, file_pointer, str_array, int_array, float_array, filename);
               } else if(strncmp(copied_line, "define", 6) == 0) {
                  file_pointer = read_c_file(filename, line);
                  //file_pointer = user_defined_fn(line, file_pointer);
               } else {
                  printf("\n\nEXECUTING THE OLD TOKENIZE");
                  tokenize_command_line(line, str_array, int_array, float_array, filename);
               }
            }
         }
         fclose(file_pointer);
      } else {
         printf("\nFile was not found");
         exit(1);
      }
   }


   /* Function to create a new thread which will read a new linda input file and tokenize and execute the commands line by line */

   /* void* create_thread_per_file(char single_line[300]) {
      char str_array[20][20];
      int int_array[20]; 
      float float_array[20];
      read_file_contents(single_line, str_array, int_array, float_array);
   } */


   /* Function to read the filenames from the meta file i.e. t10.dat */

   void get_filenames(char * meta_file) {
      // the str, int and float array are supposed to be created newly with every thread creation and are supposed to be passed to read_file_contents
      char str_array[20][20];
      int int_array[20]; 
      float float_array[20];
      FILE * mfp;
      mfp = fopen(meta_file, "r");

      if(mfp == NULL) {
          printf("\nMeta file was not found");
         exit(1);
      } else {
         char single_line[300];
         while(fgets(single_line, sizeof(single_line), mfp) != NULL){
            // call a thread create function and that function will create str, int and float arrays and send them to the read_file_contents function
            // Below read_file_contents function call should actually be replaced by create_thread_per_file(single_line);
            read_file_contents(single_line, str_array, int_array, float_array);
      }
      fclose(mfp);
    }
   }


   /* Main function - program executes from here */

   int main() {
      
      char *filename = malloc (100);

      // Initialize the global tuple space at the start of the program.
      initialize_global_tuple_space();

      //Check the values of gloabl tuple space before adding any values to it.
      //dump_global_tuple_space();
      
      printf("Enter the filename which has all the filenames of the input files:");
      scanf("%s", filename);

      get_filenames(filename);

      printf("\n\nFINAL GLOBAL TUPLE SPACE:");
      dump_global_tuple_space();
      
      return(0);
   }


